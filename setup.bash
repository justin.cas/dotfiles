#!/bin/bash

# Install ASDF
#git clone https://github.com/asdf-vm/asdf.git ~/.asdf
#cd ~/.asdf
#git checkout "$(git describe --abbrev=0 --tags)"

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
echo "export PATH=/opt/homebrew/bin:$PATH" > ~/.zshrc
echo ". $HOME/.asdf/asdf.sh" > ~/.zshrc
source ~/.zshrc

# Install homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Brew some shit up
brew install lua
brew install lua-language-server
brew install neovim
brew install ripgrep
brew install asdf
brew install powerlevel10k
brew install gsl # - allows for colored folder outs (see aliases)
brew install tmux
brew install stylua # lua formatter
brew install pgformatter # sql formatter

# Install Nerd Fonts
#https://gist.github.com/davidteren/898f2dcccd42d9f8680ec69a3a5d350e
brew tap homebrew/cask-fonts
brew search '/font-.*-nerd-font/' | awk '{ print $1 }' | xargs -I{} brew install --cask {} || true

# Create symlinks
echo "Creating all symlinks"

cd ~

ln -s ~/dotfiles/.inputrc ~/.inputrc
ln -s ~/dotfiles/.aliases ~/.oh-my-zsh/custom/aliases.zsh
ln -s ~/dotfiles/nvim/.config/nvim ~/.config/nvim

# Adding some git config
git config --global core.editor "vim"
git config --global pull.ff only

# Nvim Configuration
# - install vim-plug -> https://github.com/junegunn/vim-plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
