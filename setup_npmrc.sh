#!/bin/bash

FILE=~/.npmrc
if [ ! -f "$FILE" ]; then
  touch $FILE
fi

PASSWORD=$(echo -ne "$1:$2" | openssl base64)

echo "_auth=$PASSWORD" >> $FILE
