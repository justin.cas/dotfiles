-- Neoformat
vim.g.neoformat_try_node_exe = 1
vim.g.neoformat_enabled_javascript = { "prettier", "eslint_d" }
vim.g.neoformat_enabled_go = { "gofmt" }
vim.g.neoformat_enabled_rust = { "rustfmt" }
vim.g.neoformat_enabled_lua = { "stylua" }
vim.g.neoformat_enabled_sql = { "pg_format" }
vim.g.neoformat_enabled_yaml = { "prettier" }
vim.g.neoformat_enabled_markdown = { "prettier" }
vim.g.neoformat_enabled_json = { "prettier" }

vim.api.nvim_create_autocmd("BufWritePre", {
	pattern = "*.js,*.jsx,*.ts,*.tsx,*.rs,*.go,*.lua,*.sql,*.yml,*.md,*.json",
	group = augroup,
	command = "Neoformat",
})
