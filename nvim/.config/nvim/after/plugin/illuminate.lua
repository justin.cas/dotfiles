-- Illuminate
vim.g.Illuminate_ftblacklist = { "harpoon" }
vim.g.Illuminate_highlightUnderCursor = 1
