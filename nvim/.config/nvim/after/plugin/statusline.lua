-- Lualine
require("lualine").setup({
	options = {
		theme = "tokyonight",
		icons_enabled = false,
	},
	sections = {
		lualine_a = { "mode" },
		lualine_b = { "branch", "diff", "diagnostics" },
		lualine_c = { { "filename", path = 1 } },
		lualine_x = { "encoding", "fileformat", "filetype" },
		lualine_y = { "progress" },
		lualine_z = { "location" },
	},
})
--require('lightline').setup({
--  colorscheme = 'tokyonight',
--  active = {
--    left = {
--      { 'mode', 'paste' },
--      { 'gitbranch', 'readonly', 'filename', 'modified' }
--    }
--  },
--  component_function = {
--    gitbranch = 'gitbranch#name'
--  }
--})
--vim.g['lightline'] = {
--  colorscheme = 'one',
--  active = {
--    left = {{'mode', 'paste'}, {'gitbranch', 'readonly', 'filename', 'modified'}}
--  },
--  tabline = {
--    left = {{'buffers'}},
--    right = {{'close'}}
--  },
--  component_expand = {
--    buffers = 'lightline#bufferline#buffers'
--  },
--  component_type = {
--    buffers = 'tabsel'
--  },
--  component_function = {
--    gitbranch = 'gitbranch#name'
--  }
--}
