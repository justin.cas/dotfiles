require("nvim-treesitter.configs").setup({
	ensure_installed = {
		"lua",
		"rust",
		"toml",
		"javascript",
		"typescript",
		"go",
		"gomod",
		"gowork",
		"json",
		"yaml",
		"regex",
		"dockerfile",
	},
	auto_install = true,
	incremental_selection = { enable = true },
	textobjects = { enable = true },
	highlight = { enable = true },
	ident = { enable = true },
	--rainbow = {
	--  enable = true,
	--  extended_mode = true,
	--  max_file_lines = nil,
	--}
})
