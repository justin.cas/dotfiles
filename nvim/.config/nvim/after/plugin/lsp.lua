local Remap = require("justin.keymap")
local nnoremap = Remap.nnoremap
local inoremap = Remap.inoremap

local sumneko_binary = "/opt/homebrew/bin/lua-language-server"

-----------
-- nvim-cmp
-----------
local cmp = require("cmp")
local source_mapping = {
	buffer = "[Buffer]",
	nvim_lsp = "[LSP]",
	nvim_lua = "[Lua]",
	cmp_tabnine = "[TN]",
	copilot = "[CP]",
	path = "[Path]",
	--vsnip = "[VSnip]",
	--luasnip = "[LuaSnip]",
	--ultisnips = "[UltiSnips]",
}
local lspkind = require("lspkind")

cmp.setup({
	snippet = {
		expand = function(args)
			-- For `vsnip` user.
			-- vim.fn["vsnip#anonymous"](args.body)

			-- For `luasnip` user.
			require("luasnip").lsp_expand(args.body)

			-- For `ultisnips` user.
			-- vim.fn["UltiSnips#Anon"](args.body)
		end,
	},
	mapping = {
		["<C-p>"] = cmp.mapping.select_prev_item(),
		["<C-n>"] = cmp.mapping.select_next_item(),
		["<C-d>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<C-Space>"] = cmp.mapping.complete(),
		["<C-e>"] = cmp.mapping.close(),
		["<CR>"] = cmp.mapping.confirm({
			behavior = cmp.ConfirmBehavior.Insert,
			select = true,
		}),
	},

	-- NOTE: FiraCode glyphs
	-- https://beautifulwebtype.com/fira-code/glyphs/?i=1428
	formatting = {
		format = lspkind.cmp_format({
			preset = "codicons",
			mode = "symbol_text",
			maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
			ellipsis_char = "...", -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)

			before = function(entry, vim_item)
				-- NOTE: this line below will break the config outlined above since it directly overrides it.
				--vim_item.kind = lspkind.presets.codicons[vim_item.kind]

				local menu = source_mapping[entry.source.name]

				if entry.source.name == "copilot" then
					vim_item.kind = "⌨"
				end
				if entry.source.name == "cmp_tabnine" then
					if entry.completion_item.data ~= nil and entry.completion_item.data.detail ~= nil then
						menu = entry.completion_item.data.detail .. " " .. menu
					end
					vim_item.kind = ""
				end

				vim_item.menu = menu
				return vim_item
			end,
		}),
	},

	sources = {
		-- Copilot Source
		{ name = "copilot", group_index = 2 },
		-- TabNine Source
		{ name = "cmp_tabnine" },
		-- Other Sources
		{ name = "nvim_lsp", keyword_length = 1 },
		{ name = "vsnip", keyword_length = 2 }, -- For vsnip user.
		{ name = "path" },
		{ name = "buffer", keyword_length = 2 },
		{ name = "nvim_lua", keyword_length = 2 },
		{ name = "luasnip" }, -- For luasnip user.
		{ name = "ultisnips" }, -- For ultisnips user.
		--{ name = "nvim_lsp_signature_help"},
	},
})

local tabnine = require("cmp_tabnine.config")
tabnine:setup({
	max_lines = 1000,
	max_num_results = 5,
	sort = true,
	run_on_every_keystroke = true,
	snippet_placeholder = "..",
	show_prediction_strength = true,
})

local function config(_config)
	return vim.tbl_deep_extend("force", {
		on_attach = function()
			local opts = { buffer = true }
			nnoremap("gd", function()
				vim.lsp.buf.definition()
			end, opts)
			nnoremap("gD", function()
				vim.lsp.buf.declaration()
			end, opts)
			nnoremap("K", function()
				vim.lsp.buf.hover()
			end, opts)
			nnoremap("<leader>vws", function()
				vim.lsp.buf.workspace_symbol()
			end, opts)
			nnoremap("<leader>vd", function()
				vim.diagnostic.open_float()
			end, opts)
			nnoremap("[d", function()
				vim.diagnostic.goto_next()
			end, opts)
			nnoremap("]d", function()
				vim.diagnostic.goto_prev()
			end, opts)
			nnoremap("<leader>vca", function()
				vim.lsp.buf.code_action()
			end, opts)
			nnoremap("<leader>vco", function()
				vim.lsp.buf.code_action({
					filter = function(code_action)
						if not code_action or not code_action.data then
							return false
						end

						local data = code_action.data.id
						return string.sub(data, #data - 1, #data) == ":0"
					end,
					apply = true,
				})
			end, opts)
			nnoremap("<leader>vrr", function()
				vim.lsp.buf.references()
			end, opts)
			nnoremap("<leader>vrn", function()
				vim.lsp.buf.rename()
			end, opts)
			nnoremap("<C-k>", function()
				vim.lsp.buf.signature_help()
			end, opts)
			nnoremap("<F2>", function()
				vim.lsp.buf.rename()
			end, opts)
		end,
	}, _config or {})
end

--local function config(_config)
--	return vim.tbl_deep_extend("force", {
--		on_attach = function()
--			Nnoremap("gD", ":lua vim.lsp.buf.declaration()<CR>")
--			Nnoremap("gd", ":lua vim.lsp.buf.definition()<CR>")
--			Nnoremap("K", ":lua vim.lsp.buf.hover()<CR>")
--			Nnoremap("<leader>vws", ":lua vim.lsp.buf.workspace_symbol()<CR>")
--			Nnoremap("<leader>vd", ":lua vim.diagnostic.open_float()<CR>")
--			Nnoremap("[d", ":lua vim.lsp.diagnostic.goto_next()<CR>")
--			Nnoremap("]d", ":lua vim.lsp.diagnostic.goto_prev()<CR>")
--			Nnoremap("<leader>vca", ":lua vim.lsp.buf.code_action()<CR>")
--			Nnoremap("<leader>vrr", ":lua vim.lsp.buf.references()<CR>")
--			Nnoremap("<F2>", ":lua vim.lsp.buf.rename()<CR>")
--			Inoremap("<C-h>", "<cmd>lua vim.lsp.buf.signature_help()<CR>")
--		end,
--	}, _config or {})
--end

-------------------
-- Diagnostic Icons
-------------------
local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
	local hl = "DiagnosticSign" .. type
	vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end

----------
-- Symbols
----------
local opts = {
	highlight_hovered_item = true,
	show_guides = true,
}
require("symbols-outline").setup(opts)

-------------
-- Typescript
-------------
require("lspconfig").tsserver.setup(config())

--------
-- Astro
--------
require("lspconfig").astro.setup(config())

---------
-- Golang
---------
require("lspconfig").gopls.setup(config({
	cmd = { "gopls" },
	settings = {
		gopls = {
			experimentalPostfixCompletions = true,
			analyses = {
				unusedparams = true,
				shadow = true,
			},
			staticcheck = true,
		},
	},
}))

-------
-- Rust
-------
require("rust-tools").setup({
	tools = {
		runnables = {
			use_telescope = true,
		},
		inlay_hints = {
			auto = true,
			show_parameter_hints = true,
			parameter_hints_prefix = "",
			other_hints_prefix = "",
		},
	},

	server = {
		on_attach = config().on_attach,
		settings = {
			["rust-analyzer"] = {
				assist = {
					importEnforceGranularity = true,
					importPrefix = "crate",
				},
				checkOnSave = {
					allFeatures = true,
					command = "clippy",
				},
				imports = {
					granularity = {
						group = "module",
					},
					prefix = "self",
				},
				cargo = {
					allFeatures = true,
				},
				procMacro = {
					enable = true,
				},
				inlayHints = {
					lifetimeElisionHints = {
						enable = true,
						useParameterNames = true,
					},
				},
			},
		},
	},
})

------------
-- Lsp Lines
------------
require("lsp_lines").setup()

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
	virtual_text = false,
	--signs = true,
	--update_in_insert = true,
})
