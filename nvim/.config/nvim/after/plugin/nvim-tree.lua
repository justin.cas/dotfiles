local Remap = require("justin.keymap")
local nnoremap = Remap.nnoremap

--- nvim-tree setup
require("nvim-tree").setup({
	sort_by = "case_sensitive",
	view = {
		adaptive_size = true,
	},
	renderer = {
		group_empty = true,
	},
	filters = {
		custom = {},
	},
})

local function open_nvim_tree()
	require("nvim-tree.api").tree.open()
end

vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_nvim_tree })

nnoremap("<leader>tt", ":NvimTreeToggle<CR>")
nnoremap("<leader>tr", ":NvimTreeRefresh<CR>")
