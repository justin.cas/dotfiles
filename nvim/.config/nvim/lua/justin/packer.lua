return require("packer").startup(function(use)
	use("wbthomason/packer.nvim")

	-- LSP Plugins
	use("williamboman/mason.nvim")
	use("neovim/nvim-lspconfig")
	use("hrsh7th/cmp-nvim-lsp")
	use("hrsh7th/cmp-buffer")
	use("hrsh7th/nvim-cmp")
	use("hrsh7th/cmp-nvim-lua")
	use("hrsh7th/cmp-nvim-lsp-signature-help")
	use("hrsh7th/cmp-vsnip")
	use("hrsh7th/cmp-path")
	use("hrsh7th/vim-vsnip")
	use({ "tzachar/cmp-tabnine", run = "./install.sh", requires = "hrsh7th/nvim-cmp" })
	use("onsails/lspkind-nvim")
	use("nvim-lua/lsp_extensions.nvim")
	use("Maan2003/lsp_lines.nvim")
	use({
		"jose-elias-alvarez/null-ls.nvim",
		config = function()
			require("null-ls").setup()
		end,
		requires = { "nvim-lua/plenary.nvim" },
	})
	use({
		"zbirenbaum/copilot-cmp",
		after = { "copilot.lua" },
		config = function()
			require("copilot_cmp").setup()
		end,
	})
	use({
		"zbirenbaum/copilot.lua",
		cmd = "Copilot",
		event = "InsertEnter",
		config = function()
			require("copilot").setup({})
		end,
	})
	-- use("github/copilot.vim")

	-- Diagram
	use("jbyuki/venn.nvim")
	use("glepnir/lspsaga.nvim")
	use("simrat39/symbols-outline.nvim")

	-- Git
	--use("airblade/vim-gitgutter")
	use("lewis6991/gitsigns.nvim")
	use("ThePrimeagen/git-worktree.nvim")
	use("nvim-treesitter/nvim-treesitter", { run = ":TSUpdate" })
	use("nvim-treesitter/nvim-treesitter-context")
	use("APZelos/blamer.nvim")
	use("tpope/vim-fugitive")
	use("tpope/vim-rhubarb")
	use("tpope/vim-projectionist")
	use("tpope/vim-dispatch")
	use("junegunn/gv.vim")
	use("mbbill/undotree")
	use("theprimeagen/vim-be-good")
	use("tomlion/vim-solidity")

	--use("nvim-treesitter/playground")

	-- Debugger
	use("mfussenegger/nvim-dap")
	use("Pocco81/DAPInstall.nvim")
	use("szw/vim-maximizer")

	-- Telescope
	use("nvim-lua/popup.nvim")
	use("nvim-lua/plenary.nvim")
	use("nvim-telescope/telescope.nvim")
	use("nvim-telescope/telescope-fzy-native.nvim")

	-- Harpoon
	use("mhinz/vim-rfc")
	use("ThePrimeagen/harpoon")
	use("ThePrimeagen/refactoring.nvim")

	-- Rust
	use("simrat39/rust-tools.nvim")
	use("rust-lang/rust.vim")

	-- Go
	use("fatih/vim-go", { run = ":GoUpdateBinaries" })

	-- Formatting
	use("sbdchd/neoformat")
	use({
		"danymat/neogen",
		config = function()
			require("neogen").setup({})
		end,
		requires = "nvim-treesitter/nvim-treesitter",
		-- Uncomment next line if you want to follow only stable versions
		-- tag = "*"
	})
	use("dhruvasagar/vim-table-mode")
	--use("heavenshell/vim-jsdoc", { for = ["javascript", "javascript.jsx","typescript"], run = "make install" })

	-- Snippets
	use("L3MON4D3/LuaSnip")
	use("rafamadriz/friendly-snippets")

	-- Quality of life
	use("psliwka/vim-smoothie") -- smooth scrolling
	use("RRethy/vim-illuminate") -- cursor word highlight matching
	use("kyazdani42/nvim-web-devicons") -- optional, for file icons
	use("kyazdani42/nvim-tree.lua") -- file tree
	use("lewis6991/impatient.nvim") -- SPEEEEEEDDDD baby

	-- Theme
	use({ "catppuccin/nvim", as = "catppuccin" })
	use({ "nvim-lualine/lualine.nvim" })
	--use("itchyny/lightline.vim") -- status bar
	--use("itchyny/vim-gitbranch") -- git branch in status bar
	use("gruvbox-community/gruvbox")
	use("folke/tokyonight.nvim")
	use("thedenisnikulin/vim-cyberpunk")
	use("rebelot/kanagawa.nvim")
	-- use("martinsione/darkplus.nvim")
	-- use("tomasiser/vim-code-dark")
	-- use("joshdick/onedark.vim")
	--
	-- LOOK INTO THESE
	-- rhysd/conflict-marker.vim
	-- echasnovski/mini.nvim
end)
